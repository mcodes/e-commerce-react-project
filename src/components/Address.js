import React, { Component } from 'react'
import ShoppingCartItem from './shopping_cart_item/Shopping_Cart_Item'
import CheckOut from './checkOut/CheckOut'


class Address extends Component {

  state = {
    name: '',
    email: '',
    address: '',
    postCode: '',
    country: '',
    state: '',
    mobile: '',
    phone: '',
    display: false

  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  };

  handleSubmit = (e) => {
    e.preventDefault();
    var { lastName, email, address, postCode, country, state, mobile, phone } = this.state
    var { cart } = this.props
    const shippingData = { name: this.state.name, lastName, email, address, postCode, country, state, mobile, phone, cart  }
    //changing display to true to show button
    this.setState({ shippingData, display: true })
    //show checkout button
  };


  paymentLanding = () => {
    //mandamos la funcion a checkout, on payment success cambiamos la url a la home / en futuro pendiente crear landing
    this.props.history.push({ pathname: `/` })
  }




render() {



  console.log('FROM ADRESS===================================', this.props)
  const total = this.props.total;
  return (
    <div>
      <div className='d-around-flex'>
        <div className='address-form'>
          <h3>Shipping & Billing Address</h3>

          <form onChange={this.handleChange} onSubmit={this.handleSubmit}
            className=''
            style={{
              border: '1px solid grey',
              width: "80%",
              margin: "0 auto",
              minHeight: "50vh",
              marginBottom: "1em",
            }}>


            <input required={true}
              type="text" placeholder="Name" name="name" />

            <input required={true}
              type="text" placeholder="Last Name" name="lastName" />

            <input required={true}
              type="email" placeholder="Email" name="email" />

            <input required={true}
              type="text" placeholder="Address" name="address" />

            <input required={true}
              type="text" placeholder="City" name="city" />

            <input required={true}
              type="text" placeholder="Post Code" name="postCode" />

            <input required={true}
              type="text" placeholder="Country" name="country" />

            <input required={true}
              type="text" placeholder="State" name="state" />

            <input required={true}
              type="tel" placeholder="Mobile" name="mobile" />

            <input required={true}
              type="tel" placeholder="Phone" name="phone" />

            <button type="submit">Checkout</button>
          </form>
        </div>

        <div className='shopping-cart'>
          <h3> Overview sales order</h3>
          <div className='shopping-cart-items'>

          {this.props.cart.length!=0 ?
                    this.props.cart.map((el, index) => {
                      // let prodIndex = cartItems.findIndex(ele => ele._id === el._id)
                      // let quantity = cartItems[prodIndex].quantity;
                      return <ShoppingCartItem {...el} key={index}
                        add={this.props.add} 
                        delete={this.props.delete}
                        />
      
                      //...spread
                    })
                    : this.props.history.push({ pathname: `/` })
            }
          </div>
          <h3>Grand Total: {total}€</h3>

        </div>


      </div>
      {/* on send we */}
      {this.state.display ?
        <CheckOut
          //instead of name we can pass order number or id
          //we can find all this documentation in stripe checkout
          name={'orderNumber'}
          description={'Your order made in...'}
          //amount will come from our app total of shopping cart
          amount={total}
          label='Buy'
          shippingData={this.state.shippingData}
          paymentLanding={this.paymentLanding}
          emptyCart={this.props.emptyCart}

        />
        : null

      }

    </div>
  )
}
}

export default Address
