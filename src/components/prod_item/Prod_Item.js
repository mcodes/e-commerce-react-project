import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { far } from '@fortawesome/free-regular-svg-icons'
import '../../App.css';
// import './styles.css'
import axios from "axios"
library.add(far)
 

class Prod_Item extends Component {
    state = {
        show: 'd-none',
        color: 'secondary-color'
    }


render() {
    // console.log("from product item=", this.props)
    // console.log("from product item=", this.props.product)
    return (
        // <article onMouseEnter={() => this.show()}
        //     onMouseLeave={() => this.hide()}
        <article className='prod-item pb-4'>
            {/* onClick img cambia el url  */}
            <div className='prod-item-img cursor-pointer' onClick={() => this.props.props.history.push(`/product-view/${this.props._id}`)}><img src={this.props.img_link} /></div>
            {/* <a onClick={()=> this.props.history.push(`/product-view/cart/${this.props._id}`)} href='#'><button className = {'prod-item-button' + ' ' + this.state.show}>+ Add to Cart</button></a> */}

            {/* <h4 className={'prod-item-button cursor-pointer'} onClick={this.addToCart}>add to cart</h4> */}
            <h4 className={'prod-item-button cursor-pointer'} onClick={()=>this.props.addToCart(this.props._id, this.props.quantity)}>add to cart</h4>

            <div className='prod-item-info'>
                <p className='h3 pt-1'>{this.props.name}</p>
                <p className='h3'>{this.props.category}</p>
                <p className='h4 secondary-color d-inline'>{this.props.price}€</p>
                {/* TODO: not working */}
                <FontAwesomeIcon className='icon + {this.state.color}' icon={['far', 'heart']} onClick={()=>this.props.addToWishList(this.props._id)} />
                <h4 className='gray d-inline cursor-pointer hover-main' onClick={() => this.props.props.history.push(`/product-view/${this.props._id}`)}>See prouct details</h4>
            </div>
        </article>

    )
}
}

export default Prod_Item;