import React, { Component } from 'react';
import ShoppingCartItem from '../shopping_cart_item/Shopping_Cart_Item';
import { withRouter } from 'react-router-dom'

class Shopping_Cart extends Component {

    state = {
        products: [],
        total: 0,
    }

    //componentwillmount cogemos del localstorage
    //de local storage mandamos al server
    //




    render() {
        console.log('*************** props ******************', this.props)
        console.log('this.props.cart*************************', this.props.total)

        const cartItems = JSON.parse(localStorage.getItem('cart'))

        return (
            <div className='shopping-cart'>
                <div className='shopping-cart-items'>
                    {
                        this.props.cart.map((el, index) => {
                            // let prodIndex = cartItems.findIndex(ele => ele._id === el._id)
                            // let quantity = cartItems[prodIndex].quantity;
                            return <ShoppingCartItem {...el} key={index} //quantity={quantity}
                                add={this.props.add}
                                delete={this.props.delete} />

                            //...spread
                        })
                    }
                </div>
                {/* TODO: EXAMPLE OF CONDITIONAL STATEMENT */}
                <h4>{this.props.cart.length === 0 ? "Your shopping cart is empty." : null}</h4>
                <div className='totalAmount bold'>
                    <h4>Total (iva included)</h4>
                    <h4 className='d-inline'>{this.props.total}€</h4>
                </div>
                {this.props.cart.length >= 1 ?
                    <button onClick={() => {this.props.history.push({ pathname: `/address` });this.props.close()}}>Continue to payment</button>
                    : null
                }

            </div>
        )
    }
}

export default withRouter(Shopping_Cart);
