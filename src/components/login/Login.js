import React from 'react'
import axios from 'axios';
import {withRouter} from 'react-router-dom'

const URL = `http://localhost:8080/users/login`


class Login extends React.Component {
    state = {
        email: '',
        password: ''
    }

    componentWillMount() {
        //queremos ver si hay token (osea esta logueado)
        //antes miramos si existe en el local storage
        //si hay token lo mandamos a la home page

        //en token metemos lo q tenemos en authToken
        const token = JSON.parse(localStorage.getItem('authToken'));
        //si existe
        if (token != null)

            axios.get(`http://localhost:8080/users/verifyToken/${token}`)

                .then((res) => {
                    //if ok is true
                    if (res.data.ok) {
                        this.props.history.push(`/`)

                    }
                })
                .catch(e => {
                    console.log('error', e)
                })


    }


    handleChange = e => this.setState({ [e.target.name]: e.target.value })
    handleSubmit = e => {
        let { email, password } = this.state
        e.preventDefault()
        fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                //debugger
                console.log(responseJson)
                if (responseJson.ok) {
                    localStorage.setItem('authToken', JSON.stringify(responseJson.token));
                    console.log('Welcome back ' + responseJson.email);
                    this.props.isLoggedIn()
                    this.props.history.push(`/`)
                } else {
                    console.log(responseJson.message)
                }

            }).catch((e) => {
                // debugger
            })
    }

    render() {
        return (
            <form
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                className="register_form">
                <input
                    name='email'
                    placeholder="email"
                />
                <input
                    name='password'
                    placeholder='password'
                />
                <button>Login</button>
                <h5>Don't have an account yet? <span className="underline cursor-pointer" onClick={() => this.props.history.push(`/register`)}>Click here to Register in 30 seconds!</span></h5>
            </form>
        )
    }
}
export default withRouter (Login)