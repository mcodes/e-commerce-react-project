import React, { Component } from 'react';
import Prod_List from './prod_list/Prod_List';
// import logo from './logo.svg';
// import '../App.css';
import axios from 'axios';


class Home extends Component {
   



    
    render() {
        // console.log("HOMEPROPS",this.props)
        return (
            <div className="App">
                <main>
                    <section className='top-banner'>
                        <div className="top-banner-txt prl-1">
                            <h2 className='ptb-1'>Looking for a unique design?</h2>
                            <p className='pb-1'>Contact us for personalized designs, created specialy for you.</p>
                        </div>
                    </section>
                    {/* passing props of router to prodlist and this.state.products */}
                    <Prod_List {...this.props} 
                    addToWishList = {this.props.addToWishList}
                    addToCart = {this.props.addToCart}
                    products = {this.props.products}
                    />
                </main>
            </div>
        );

    }
}

export default Home;
