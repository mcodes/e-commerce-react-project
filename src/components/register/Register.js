import React from 'react'
import axios from 'axios';


const URL = `http://localhost:8080/users/register`


export default class Register extends React.Component {
    state = {
        email: '',
        password: '',
        password_2: ''
    }

    componentWillMount() {
        //queremos ver si hay token (osea esta logueado)
        //antes miramos si existe en el local storage
        //si hay token lo mandamos a la home page

        //en token metemos lo q tenemos en authToken
        const token = JSON.parse(localStorage.getItem('authToken'));
        //si existe
        if (token != null)

            axios.get(`http://localhost:8080/users/verifyToken/${token}`)

                .then((res) => {
                    //if ok is true
                    if (res.data.ok) {
                        this.props.history.push(`/`)

                    }
                })
                .catch(e => {
                    console.log('error', e)
                })


    }

    handleChange = e => this.setState({ [e.target.name]: e.target.value })

    handleSubmit = e => {
        let { email, password, password_2 } = this.state

        e.preventDefault()
        fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
                password_2
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                debugger
                if (responseJson.ok) {
                    setTimeout(() => {
                        this.props.history.push('/login')
                    }, 3000)
                }

            }).catch((e) => {
                debugger
            })
    }

    render() {
        return (
            <form
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                className="register_form">

                <input
                    name='email'
                    placeholder="email"
                />
                <input
                    name='password'
                    placeholder='password'
                />
                <input
                    name='password_2'
                    placeholder='password_2'
                />
                <button>Register</button>
                <h5><span className="underline cursor-pointer" onClick={() => this.props.history.push(`/login`)}>Already have an account? Click here to Log in!</span></h5>
            </form>

        )
    }
}
