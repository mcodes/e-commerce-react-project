import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { far } from '@fortawesome/free-regular-svg-icons'
import '../../App.css';
// import './styles.css'
import axios from "axios"
import { withRouter } from 'react-router-dom'
library.add(far)



class AddToCartButton extends Component {
    state = {}

    // show = () => this.setState({ show: 'd-block' })
    // hide = () => this.setState({ show: 'd-none' })

    


    render() {
        return (
            <h4 className={'prod-item-button cursor-pointer'} onClick={this.props.addToCart}>add to cart</h4>
        )
    }
}

export default withRouter(AddToCartButton);