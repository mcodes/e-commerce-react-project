import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { far } from '@fortawesome/free-regular-svg-icons'
import AddToCartButton from '../addToCartButton/AddToCartButton'

library.add(far)


class Wish_List_Item extends Component {
    state = {
    }


    render() {

        const width100 = {
            width: '100%',
        };

        return (
            <div className='shopping-cart-item'>
                <div className='shopping-cart-item-img'><img style={width100} src={this.props.img_link} /></div>
                {/* <div className='shopping-cart-item-img'><img src="https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30094.jpg" /></div> */}
                <div>
                    {/* <h3 className=' pt-1'>Greenhill circular marble table</h3> */}
                    <h3 className=' pt-1'>{this.props.name}</h3>
                    <h3 className='d-inline '>{this.props.price}€</h3>
                    <AddToCartButton
                        _id={this.props._id}
                        quantity={1}
                    />
                </div>
                <hr />
            </div>

        )
    }
}

export default Wish_List_Item;