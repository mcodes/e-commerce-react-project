import React, { Component } from 'react';
import Prod_Item from '../prod_item/Prod_Item';
import Fake_Prod from '../prod_list/fake_products';

import '../../App.css';




class Prod_List extends Component {
    render() {

        console.log("from prod_list=", this.props)

        return (
            <section className='prod-list'>
                {
                    // Fake_Prod.map((el,index)=>{
                    this.props.products.map((el, index) => {
                        //pasamos los props del router al prod item {...this.props}
                        return <Prod_Item {...this.props} {...el} key={index}
                            addToWishList={this.props.addToWishList}
                            addToCart={this.props.addToCart}

                        />
                        //...spread
                    })
                }
            </section>
        )
    }
}

export default Prod_List;
