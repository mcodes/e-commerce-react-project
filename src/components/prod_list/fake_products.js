//OJO IN MONGO CHANGED PROD_NAME TO NAME 
 
 const products = [
    {
    prod_id:1, 
    prod_name: 'Greenhill circular marble table',
    price: 500,
    stock: 2,
    category: 'dining table',
    material: 'wood/iron',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: 'jfjfkfkfkfkf',
    description_short: 'jfjfkfkfkfkf',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_29516.jpg'

},
{
    prod_id:2, 
    prod_name: 'Coffee table Paradigm',
    price: 129,
    stock: 2,
    category: 'side table',
    material: 'cristal/metal',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: "Marble and gold are VIPs in the chic decor. That's why so tableside Shefflied will be the star of your living room. With over white marble and gilded structure, this piece will be a touch of lightness and style, with matching side table, double its trendy effect.",
    description_short: 'Side table Sheffield Ø 43 cm with marble top and golden metal structure.',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30413.jpg'

},
{
    prod_id:3, 
    prod_name: 'Side table Sheffield',
    price: 214,
    stock: 2,
    category: 'coffee table',
    material: 'wood/iron',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: "Original and sophisticated design Paradigm table is inspired by Art Deco. Twenties are back! His look is so versatile that you'll love with any style as contemporary or vintage. We encourage you to take a step further and try a groundbreaking style as bohemian chic.",
    description_short: 'Side table Sheffield Ø 80 cm with marble top and golden metal structure.',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30094.jpg'

},
{
    prod_id:4, 
    prod_name: 'Kyfler coffee table',
    price: 500,
    stock: 2,
    category: 'coffee table',
    material: 'wood/iron',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: 'The versatility of the Kyler table is as much in its design as in its matte color. With its match Black & Wood, it fits in all environments, especially those where the Milian collection is, with the same matt black finish. Throw yourself into a total look.',
    description_short: 'Take the Kyler coffee table anywhere in the house. Ideal for small spaces, this metal table painted in matt black will delight you as it combines with wood, thanks to the acacia finish of each of its legs. Enjoy it everywhere.',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30402.jpg'

},
{
    prod_id:5, 
    prod_name: 'Pearson Coffee Table',
    price: 73,
    stock: 2,
    category: 'side table',
    material: 'wood/metal',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: "The versatility of the Pearson table is as much in its design as in its black and copper match, fitting in all the environments, especially those where the Milian collection is, of the same matt black finish. Throw yourself into a total look.",
    description_short: 'Take the Pearson side table anywhere in the house. Ideal for small spaces, this table sports a round metal envelope painted in matt black, covering the edge with copper-colored metallic paint. This envelope, in addition, is supported by three aluminum legs. Enjoy it everywhere',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30411.jpg'

},
{
    prod_id:6, 
    prod_name: 'Round Coffee Table Mathis',
    price: 114,
    stock: 2,
    category: 'coffee table',
    material: 'wood/iron',
    color: 'black',
    dimensions: 'H 40 cm x L 80 cm x W 80 cm',
    weight: 18.30,
    description_long: "The clean lines of the Mathis tables will bring a touch of sophistication to a prominent space in your living room or on your terrace. Emulating cement, this piece stands out for its unpretentious authenticity. The design itself conveys a simple elegance that defines classy decor, returning to minimalist lines. It is a piece of furniture you can further enhance with different styles of accessories. It never fails.",
    description_short: 'Coffee table with galvanized steel powder coated structure. Top in poly-cement. For indoor and outdoor under cover use.',
    img_link: 'https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/c/c/cc0707r01_4a.jpg'

},
]

export default products