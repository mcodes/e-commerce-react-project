import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';


// import './styles.css';

library.add(faTrashAlt)


class Shopping_Cart_Item extends Component {
    state = {}

    

    render() {

        const width100 = {
            width: '100%',
          };

        console.log("from product item= props", this.props)
        console.log("from product item this.props.product=", this.props.product)
        console.log("from product item= quantity", this.props.quantity)
        
        return (
            <div className='shopping-cart-item'>
                <div className='shopping-cart-item-img'><img style={width100} src={this.props.img_link} /></div>
                {/* <div className='shopping-cart-item-img'><img src="https://media.kavehome.com/media/catalog/product/cache/6/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_30094.jpg" /></div> */}
                <div>
                    {/* <h3 className=' pt-1'>Greenhill circular marble table</h3> */}
                    <h5 className=' pt-1 d-inline'>{this.props.name} | {this.props.price}€ / u.</h5>
                    <FontAwesomeIcon onClick={()=> this.props.delete(this.props._id)} className='icon cursor-pointer' icon='trash-alt'/>
                    <div className='bold h4'><span className="pr-1 cursor-pointer" onClick={() => this.props.add("-", this.props._id)}>-</span>{this.props.quantity}<span className="pl-1 pr-1 cursor-pointer" onClick={() => this.props.add("+", this.props._id)}>+</span></div>
                    <h5 className='d-inline'>{this.props.price * this.props.quantity}€</h5>
                </div>
                <hr/>
            </div>
        )
    }
}

export default Shopping_Cart_Item;