import React, { Component } from 'react';
import WishListItem from '../wish_list_item/Wish_List_Item';
import axios from 'axios';

class WishList extends Component {

    state = {
        products: [],
        total: 0,
        message:""
    }
    //componentwillmount cogemos del localstorage
    //de local storage mandamos al server
    //


    // cuando comp se renderice we get the info from the db
    componentWillMount() {
        //recuperamos array de localstorage
        const wishList = JSON.parse(localStorage.getItem('wishList'))
        //con axios mandamos el array en localstorage para buscar info segun el id
        //cartItems el obj q contiene cartItems es el body del request que pasamos al server
        //enviamos array de id (wishList) a servidor
        axios.post(`http://localhost:8080/products/wish_list_items`, { wishList })
            //la respuesta del servidor el shoppingCartItemsInfo            del server====>   res.send({shoppingCartItemsInfo});

            .then((res) => {
                //console.log('res====================================', res.data)
                // debugger
                //if res from server is message
                if(res.data.message) {
                    this.setState({message : res.data.message})

                } else {
                    this.setState({ products: res.data.wishListItemsInfo })
                }
            })
            .catch(e => {
                console.log('error', e)
            })
    }

    render() {
        //const wishListItems = JSON.parse(localStorage.getItem('wishListItems'))
       console.log('******************************************************************',this.state.products)
        return (
            <div className='wish-list'>
                {
                    this.state.products.map((el, index) => {
                        return <WishListItem {...el} key={index}
                        
                        />
                        //...spread
                    })
                }
            { this.state.message }    
                
            </div>
                )
            }
        }
        
        export default WishList;
