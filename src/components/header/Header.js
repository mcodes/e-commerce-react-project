import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobe, faUser, faHeart, faShoppingCart, faSearch } from '@fortawesome/free-solid-svg-icons'
import ModalSemantic from '../../pages/ModalSemantic'
import { tsPropertySignature } from '@babel/types';
import { withRouter } from 'react-router-dom'


library.add(faGlobe, faUser, faHeart, faShoppingCart, faSearch)

class Header extends Component {


    render() {
        // let shoppingCart = 0;
        console.log(this.props.isLoggedInState,'this.props.isLoggedInState')
        // console.log('FROM HEADER===================================',this.props)
        return (
            <header>
                <div className='promo-lang-header ptb-05'>
                    <div className='promo'>Delivery within 10 days | Free shipping in Peninsula <span className='underline'>+info</span></div>
                    <div className='lang pr-2'><FontAwesomeIcon className='icon cursor-pointer ' icon='globe' />English</div>
                </div>
                <nav className='main-navigation prl-4'>
                    <div className='search'><input placeholder='search' /><FontAwesomeIcon className='icon cursor-pointer '
                        icon='search' /></div>
                    {/* <img className='logo' src='https://mms.businesswire.com/media/20170504006119/en/584874/5/crate_logo.jpg' alt='logo'/> */}
                    <div className='logo cursor-pointer' onClick={() => this.props.history.push(`/`)}>Ferrero&Co</div>
                    <div className='header-navigation'>
                        
                        {

                           
                        !this.props.isLoggedInState
                        ? <div className='cursor-pointer d-inline' onClick={() => this.props.history.push(`/login`)} >
                            Account Sign In<FontAwesomeIcon className='icon' icon='user' />
                        </div>

                        :<div className='cursor-pointer d-inline' onClick={() => {localStorage.removeItem('authToken'); this.props.isLoggedIn()} } >
                        Sign Out<FontAwesomeIcon className='icon' icon='user' />
                    </div>
                    
                    }
                        <div className='d-inline  wish-list-heart'>
                            <FontAwesomeIcon onClick={() => this.props.history.push(`/wish-list`)}
                                className=' icon cursor-pointer main-color' size="lg" icon='heart' /><p className='wish-list-quantity'>{this.props.wishListItems}</p>
                        </div>
                        <ModalSemantic style={{position:'absolute'}}
                            add={this.props.add}
                            delete={this.props.delete}
                            cart={this.props.cart}
                            getTotal={this.props.getTotal}
                            total={this.props.total}
                            getData={this.props.getData}

                            {...this.props}
                        /><span className='cart-list-quantity'>{this.props.cartListItems}</span>
                        {/* <span>{shoppingCart}</span> */}
                    </div>

                </nav>
            </header>
        )
    }
}

//aqui no tengo las props del Router con eso se la damos.
export default withRouter(Header);