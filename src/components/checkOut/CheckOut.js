import React from 'react'
import axios from 'axios';

// import stripe checkout first
import StripeCheckout from 'react-stripe-checkout';


// that's your publishable jey obtained form strip, for testing use testing key
const STRIPE_PUBLISHABLE = 'pk_test_DRZyqLjG0nS71SA1NB4TNiTe00MwClhXl3';

// this will be the route to hit on the https://localhost:8080 after getting token from stripe
const PAYMENT_SERVER_URL = ' http://localhost:8080/orders/purchase';

//ojo currency is important!!!!!
const CURRENCY = 'EUR';

//amount always has to be multiplied by 100
//en los tpv marcamos 10000 para 100 => 100.00
const fromEuroToCent = amount => amount * 100;

const successPayment = data => {
	alert('Payment Successful');
	console.log(sendShippingData);
	//on succesful we call function createrOrder que a la vez (en el server crea order nuevo, envia email, y updates el stock)
	createOrder()
};

const errorPayment = data => {
	alert('Payment Error');
	console.log(data);
};

const createOrder = () => {
	axios.post(`http://localhost:8080/orders/create`, { sendShippingData })
		//la respuesta del servidor el shoppingCartItemsInfo            del server====>   res.send({shoppingCartItemsInfo});
		.then((res) => {
			console.log('res', res)
			goToEmptyCart()
			changePaymentLanding()
		}).catch(e => {
			console.log('error', e)
		})
}

var sendShippingData;
var changePaymentLanding;
var goToEmptyCart;

const onToken = (amount, description) => token =>
	axios.post(PAYMENT_SERVER_URL,
		{
			description,
			source: token.id,
			currency: CURRENCY,
			amount: fromEuroToCent(amount)
		})
		.then(successPayment)
		.catch(errorPayment);

//aqui podemos recibir informacion del componente anterior address
// pasamos funcion como argumento y metemos en una variable
const Checkout = ({ name, description, amount, label, shippingData, paymentLanding, emptyCart }) => {
	sendShippingData = shippingData
	changePaymentLanding = paymentLanding
	goToEmptyCart = emptyCart
	debugger
	return <StripeCheckout
		name={name}
		description={description}
		amount={fromEuroToCent(amount)}
		token={onToken(amount, description)}
		currency={CURRENCY}
		stripeKey={STRIPE_PUBLISHABLE}
		label={label}
		data={'data'}

	/>
}
export default Checkout;