import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fab)

class Footer extends Component {

    // handleSubmit = (e) => {
    //     e.preventDefault();
    //     let todo = this.refs.todo.value;
    //     if (todo) {
    //         this.props.getData(todo)
    //         // emtpy imput
    //         this.refs.todo.value = ""
    //     }
    // }


    render() {

        return (
            <footer>
                <div className='footer-newsletter ptb-2'>
                    <span className='pr-2'>Sign up for offers, sales and news!</span>
                    <input className='mr-05' type='email' placeholder='Enter your email address' />
                    <button>Subscribe</button>
                </div>
                <div className='footer-main'>
                    <div>
                        <h3>Our Company</h3>
                        <ul>
                            <li>About Us</li>
                        </ul>
                    </div>

                    <div>
                        <h3>Resources</h3>
                        <ul>
                            <li>Customer Service</li>
                            <li>Gift Cards</li>
                            <li>Catalogs</li>
                            <li>Delivery Information</li></ul>
                    </div>

                    <div>
                        <h3>Get in touch</h3>
                        <ul>
                            <li>Send us an email</li>
                            <li>+34 938 325 516</li>
                            <li>Mon - Thur: 10 am - 8 pm CT</li>
                            <li>Fri- Thur: 10 am - 2 pm CT</li>
                            <li>Book a meeting/call with us</li>
                        </ul>
                    </div>
                    <div>
                        <h3>Social Media</h3>
                        <ul className='social-media-icons'>
                    
                            <a href='#'><FontAwesomeIcon className='icon' icon={['fab', 'facebook-f']}/></a>
                            <a href='#'><FontAwesomeIcon className='icon' icon={['fab', 'instagram']}/></a>
                            <a href='#'><FontAwesomeIcon className='icon' icon={['fab', 'pinterest']}/></a>
                            <a href='#'><FontAwesomeIcon className='icon' icon={['fab', 'youtube']}/></a>
                            
                        </ul>
                    </div>
                </div>
                <div className="footer-copyright">
                    <p><span className='bold'>Ferrero&Co</span> ©2019 Crate and Barrel. All rights reserved. Terms of Use Privacy Site Index Ad Choices Co-Browse</p>
                </div>

            </footer>
        )
    }
}

export default Footer;