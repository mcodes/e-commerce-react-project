import React, { Component } from 'react'
import { Button, Header, Image, Modal } from 'semantic-ui-react'
import axios from 'axios';
import Shopping_Cart from '../components/shopping_cart/Shopping_Cart.js'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobe, faUser, faHeart, faShoppingCart, faSearch } from '@fortawesome/free-solid-svg-icons'
// import "./styles.css"

library.add(faGlobe, faUser, faHeart, faShoppingCart, faSearch)

class ModalExampleDimmer extends Component {
  state = { open: false }

  //LLAMAMOS GETDATA ASI ACTUALIZAMOS LA INFO  
  show = dimmer => () => this.setState({ dimmer, open: true },()=> this.props.getData())
  close = () => this.setState({ open: false })

  render() {
    const { open, dimmer } = this.state
    // console.log('FROM MODALSEMANTIC===================================',this.props)
    return (
      <div className='d-inline'>

        <FontAwesomeIcon onClick={this.show('inverted')} className='icon cursor-pointer ' icon='shopping-cart' />

        {/* <div onClick={this.show('inverted')}><FontAwesomeIcon  className='icon cursor-pointer ' icon='shopping-cart' />{faShoppingCart}</div> */}

        <Modal className='modal-position' dimmer={dimmer} open={open} onClose={this.close}>
          <Modal.Content >
            <Shopping_Cart
              add={this.props.add}
              delete={this.props.delete}
              cart={this.props.cart}
              getTotal={this.props.getTotal}
              total={this.props.total}
              close={this.close}
            />

          </Modal.Content>
        </Modal>
      </div>
    )
  }
}

export default ModalExampleDimmer