import React from 'react'
import axios from 'axios'
import ReactDom from 'react-dom';

//we install mailer
//we add new route etc on our client and server


class Create extends React.Component {
    state = {
        showingAlert: false,
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
        console.log(this.state)
    }

    handleSubmit= (e) => {
        // var that = this
        e.preventDefault()

        var { prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category } = this.state
        
        // sending request to localhost
        axios.post('http://localhost:8080/products/create', { prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category })
            //data object is sent to server
            .then((response) => {
                this.setState({
                    prod_id: "",
                    name: "",
                    price: "",
                    stock: "",
                    material: "",
                    color: "",
                    dimensions: "",
                    weight: "",
                    description_long: "",
                    description_short: "",
                    img_link: "",
                    category: ""
                })
                this.setState({ showingAlert: true },()=>{
                    setTimeout(() => this.setState({showingAlert: false}), 2000);
                })     
            })
            .catch(function (error) {
                console.log(error);
            })
        console.log("--SeNd!--")
    }

    render() {
        return (
            <div className='dashboard-container form-container'>

                <h4 className='pb-2 float-left'>New Product</h4>
                {/* {
                    this.state.showingAlert === true ? <h4 className='main-color float-right'>Product added correctly</h4>
                    : null
                } */}
                
                    <h4 className={`alert alert-success ${this.state.showingAlert ? 'alert-shown' : 'alert-hidden'} main-color float-right`}>Product added correctly</h4>
                
                <div className='float-clear'>
                    <form className='admin-form' onSubmit={this.handleSubmit} onChange={this.handleChange}
                        style={{ }}>

                        <label>Prod_id</label>
                        <input required={true} value={this.state.prod_id}
                            type="number" placeholder="Product id" name="prod_id" />

                        <label>Name</label>
                        <input required={true} value={this.state.name}
                            type="text" placeholder="Product Name" name="name" />

                        <label>Price</label>
                        <input required={true} value={this.state.price}
                            type="number" placeholder="Product price" name="price" />

                        <label>Img. Link</label>
                        <input required={true} value={this.state.img_link}
                            type="text" placeholder="Product img url" name="img_link" />

                        <label>Stock</label>
                        <input required={true} value={this.state.stock}
                            type="number" placeholder="Product stock" name="stock" />


                        <label>Material</label>
                        <input required={true} value={this.state.material}
                            type="text" placeholder="Product material" name="material" />

                        <label>Color</label>
                        <input required={true} value={this.state.color}
                            type="text" placeholder="Product color" name="color" />

                        <label>Dimensions</label>
                        <input required={true} value={this.state.dimensions}
                            type="text" placeholder="Product dimensions" name="dimensions" />

                        <label>Weight</label>
                        <input required={true} value={this.state.weight}
                            type="number" placeholder="Product weight" name="weight" />


                        <label>Category</label>
                        <input required={true} value={this.state.category}
                            type="text" placeholder="Product category" name="category" />


                        <div className='comment-box'>


                            <label>Long Description</label>
                            <textarea required={true} value={this.state.description_long}
                                type="text" placeholder="Product long description" name="description_long" />

                            <label>Short Description</label>
                            <textarea required={true} value={this.state.description_short}
                                type="text" placeholder="Product short description" name="description_short" />
                            <div><img src='https://media.kavehome.com/media/catalog/product/cache/3/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_26871.jpg' /></div>
                        </div>
                        <button className='button' type="submit" label="Send">Add product</button>
                    </form>



                </div>
            </div >
        )
    }
}

export default Create