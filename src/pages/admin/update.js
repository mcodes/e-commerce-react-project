import React from 'react'
import axios from 'axios'
import ReactDom from 'react-dom';

//we add new route etc on our client and server

class Update extends React.Component {

    state = {
        quantity: 0,
        stock: true,
        showingAlert: false,
        product: {
            name: ''
        }
    }

    //cuando comp se renderice we get the info from the db
    componentWillMount() {
        const { _id } = this.props.match.params;
        //get de url path de db
        console.log('============================', this.props)
        //con axios hacemos llamada al server y tenemos q encontrar path con ese url en el server
        //en server tenemos la ruta con una funcion
        axios.get(`http://localhost:8080/products/admin/update/${_id}`)
            .then((res) => {
                //productinfo es toda la info del producto
                if (res.data.ok) this.setState({ product: res.data.productInfo })
                //we are setting the state in home to res.data so we can send them to prodlist
                //this.setState({products: res.data})
            })
            .catch(e => {
                console.log('error', e)
            })
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
        console.log(this.state)
    }

    handleSubmit(event) {
        event.preventDefault()
        const { _id } = this.props.match.params;
        var { prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category } = this.state
        // sending request to localhost, in production could be just /sendEmail
        axios.post('http://localhost:8080/products/update', { _id, prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category })
            //data object is sent to server
            .then((response) => {
                this.setState({ showingAlert: true },()=>{
                    setTimeout(() => {this.setState({showingAlert: false},()=>this.props.history.push({ pathname: `/admin/dashboard` }))}, 2000);
                    
                })

                console.log("Your product has been added", response)
            })
            .catch(function (error) {
                console.log(error);
            })
        console.log("--SeNd!--")
    }

    render() {
        return (
            <div className='dashboard-container form-container'>

                <h4 className='pb-2 float-left'>Update Product</h4>

                <h4 className={`alert alert-success ${this.state.showingAlert ? 'alert-shown' : 'alert-hidden'} main-color float-right`}>Product updated correctly</h4>

                <div className='float-clear'>
                    <form className='admin-form' onSubmit={this.handleSubmit.bind(this)} onChange={this.handleChange}
                        style={{
                            // border: '1px solid grey',
                            // width: "80%",
                            // margin: "0 auto",
                            // minHeight: "50vh",
                            // marginBottom: "1em",
                        }}>
                        {/* TODO: poner en el value el stat para borrarlos  */}

                        <label>Prod_id</label>
                        <input value={this.state.prod_id}
                            type="number" placeholder={this.state.product.prod_id} name="prod_id" />

                        <label>Name</label>
                        <input value={this.state.name}
                            type="text" placeholder={this.state.product.name} name="name" />

                        <label>Price</label>
                        <input value={this.state.price}
                            type="number" placeholder={this.state.product.price} name="price" />

                        <label>Img. Link</label>
                        <input value={this.state.img_link}
                            type="text" placeholder={this.state.product.img_link} name="img_link" />

                        <label>Stock</label>
                        <input value={this.state.stock}
                            type="number" placeholder={this.state.product.stock} name="stock" />


                        <label>Material</label>
                        <input value={this.state.material}
                            type="text" placeholder={this.state.product.material} name="material" />

                        <label>Color</label>
                        <input value={this.state.color}
                            type="text" placeholder={this.state.product.color} name="color" />

                        <label>Dimensions</label>
                        <input value={this.state.dimensions}
                            type="text" placeholder={this.state.product.dimensions} name="dimensions" />

                        <label>Weight</label>
                        <input value={this.state.weight}
                            type="number" placeholder={this.state.product.weight} name="weight" />


                        <label>Category</label>
                        <input value={this.state.category}
                            type="text" placeholder={this.state.product.category} name="category" />


                        <div className='comment-box'>


                            <label>Long Description</label>
                            <textarea value={this.state.description_long}
                                type="text" placeholder={this.state.product.description_long} name="description_long" />

                            <label>Short Description</label>
                            <textarea value={this.state.description_short}
                                type="text" placeholder={this.state.product.description_short} name="description_short" />
                            <div><img src='https://media.kavehome.com/media/catalog/product/cache/3/image/700x/040ec09b1e35df139433887a97daa66f/i/m/image_4_26871.jpg' /></div>
                        </div>
                        <button className='button' type="submit" label="Send">Update product</button>
                    </form>



                </div>
            </div >
        )
    }
}

export default Update