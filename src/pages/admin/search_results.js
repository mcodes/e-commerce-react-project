import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import '../../App.css';
// import './styles.css'
import axios from "axios"
library.add(faTrashAlt)


class SearchResults extends Component {
    state = {
        show: 'd-none'
    }


    // show = () => this.setState({ show: 'd-block' })
    // hide = () => this.setState({ show: 'd-none' })


    render() {
        console.log("from product item=", this.props)
        console.log("from product item=", this.props.product)
        return (
            <article className='search-results cursor-pointer  '>
                {/* onClick img cambia el url  */}
            <div className=' search-results-info tooltip' onClick={() => this.props.history.push(`/admin/update/${this.props._id}`)}>
                <span class="tooltiptext">Click to update</span>
                <h5 className='d-inline'>{this.props.name} | {this.props.category} | {this.props.price}€ | {this.props.stock}u.</h5>
                {/* <h5 className='gray d-inline cursor-pointer hover-main' onClick={() => this.props.history.push(`/product-view/${this.props._id}`)}>See prouct details</h5> */}
                <img src={this.props.img_link} />
                {/* <a href='#'><FontAwesomeIcon className='icon' icon={['far', 'heart']} /></a> */}
            </div>
                <div className='icon icon-alert' onClick={()=>this.props.deleteProduct(this.props._id)}><FontAwesomeIcon icon='trash-alt' /></div>
            </article>
        )
    }
}

export default SearchResults;