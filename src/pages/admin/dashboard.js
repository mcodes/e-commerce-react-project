import React, { Component } from 'react';
import SearchResults from './search_results'
import axios from 'axios';

class Dashboard extends Component {

    state = {
        //if we get undefined set the var to empty arr
        products: []
    }

    
        componentDidMount() {
           this.isAdmin()
        }

        
    isAdmin = () => {
        //en token metemos lo q tenemos en authToken
        const token = JSON.parse(localStorage.getItem('authToken'));
        if (token) {
            
            //si hay token controlas si es admin
            axios.get(`http://localhost:8080/users/verifyToken/${token}`)

                .then((res) => {
                    //if ok is true we grant access to admin and render all products
                    if (res.data.admin) return this.props.getProducts()
                    this.props.history.push(`/`)
                })
                .catch(e => {
                    console.log('error', e)
                })

        } else {
            //si no hay token no entras
            this.props.history.push(`/`)
        }
    }

    // findProducts = () => {
    //     //get de url path de db
    //     axios.get('http://localhost:8080/products/')
    //         .then(res => {
    //             console.log('res', res)
    //             //we are setting the state in home to res.data so we can send them to prodlist
    //             this.setState({ products: res.data })
    //         })
    //         .catch(e => {
    //             console.log('error', e)
    //         })
    // }


    



    render() {

        return (
            <section className='dashboard-container pt-2'>
                <div className='container-btns'>

                    <h4 className='allProducts-btn'>All products</h4>

                    <h4 className='main-color cursor-pointer ' onClick={() => this.props.history.push(`/admin/create`)}><span className='h4 pr-05 '>+ </span> Create New Product</h4>
                </div>
                {
                    this.props.products.map((el, index) => {
                        //pasamos los props del router al prod item {...this.props}
                        return <SearchResults {...this.props} {...el} key={index} deleteProduct={this.props.deleteProduct} />
                        //...spread
                    })
                }
                <h4 className='main-color cursor-pointer ' onClick={() => this.props.history.push(`/admin/createcat`)}><span className='h4 pr-05 '>+ </span> Create New Category</h4>
                {/* <Delete /> */}

            </section >
        )
    }
}

export default Dashboard;
