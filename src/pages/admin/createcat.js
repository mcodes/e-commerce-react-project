import React from 'react'
import axios from 'axios'


class Createcat extends React.Component {
    
        state = {
            categories: [],
            category:'',
        }


        componentWillMount() {
            this.getCategories()
        }

        getCategories= () => {
            axios.get('http://localhost:8080/categories/')
            .then((res)=>{
                debugger
                this.setState({ categories:res.data })
            })
            .catch(e => {
                console.log('error', e)
            })
        }


        handleChange = (e) => {
            this.setState({ [e.target.name]: e.target.value })
            console.log(this.state)
        }

        handleSubmit = (e) => {
            e.preventDefault()
            var { category } = this.state
            axios.post('http://localhost:8080/categories/create', { category })
            .then((res) => {
                this.setState({ 
                    category: '' 
                })
                this.getCategories()
                this.setState({ showingAlert: true }, () => {
                    setTimeout(() => this.setState({ showingAlert: false }), 2000);
                })
            })
                .catch(function (error) {
                    console.log(error);
            })
            console.log("--SeNd!--")
        }


        render() {

        return (
            <div className='dashboard-container form-container' >

                <h4 className='pb-2 float-left'>New Category</h4>

                <h4 className={`alert alert-success ${this.state.showingAlert ? 'alert-shown' : 'alert-hidden'} main-color float-right`}>Category added correctly</h4>

                <div className='float-clear'>
                    <form className='admin-form' onSubmit={this.handleSubmit} onChange={this.handleChange}
                        style={{}}>

                        <label>New Category</label>
                        <input onChange={this.handleChange} required={true} value={this.state.category}
                            type="text" placeholder="New Category" name="category" />


                        <button className='button' type="submit" label="Send">Add category</button>
                    </form>
                    {
                        this.state.categories.map((ele)=>{
                            return <div>{ele.category}</div>
                        })
                    }
                </div>
            </div >



        )
    }
}

export default Createcat