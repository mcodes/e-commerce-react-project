import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShareAlt } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import axios from 'axios';
import AccordionSemantic from './AccordionSemantic.js'
import AddToCartButton from '../components/addToCartButton/AddToCartButton'




library.add(fab, far, faShareAlt)

class Prod_View extends Component {
    state = {
        quantity: 1,
        stock: true,
        product: {
            name: ''
        }
    }

    //cuando comp se renderice we get the info from the db
    componentWillMount() {
        const { _id } = this.props.match.params;
        //get de url path de db
        console.log('============================', this.props)
        //con axios hacemos llamada al server y tenemos q encontrar path con ese url en el server
        //en server tenemos la ruta con una funcion
        axios.get(`http://localhost:8080/products/${_id}`)
            .then((res) => {
                console.log('res', res.data.productInfo)
                //productinfo es toda la info del producto
                if (res.data.ok) this.setState({ product: res.data.productInfo, stock: res.data.stock})
                //we are setting the state in home to res.data so we can send them to prodlist
                //this.setState({products: res.data})
            })
            .catch(e => {
                console.log('error', e)
            })
    }


    addQuantity = (sign) => {
        // debugger
        let { quantity } = this.state
        this.setState({quantity : eval(`${quantity} ${sign} 1`)},()=>{
            if( this.state.quantity === 0){
                this.setState({quantity:1})
            } 
            else if ( this.state.quantity  > this.state.product.stock ){ //-quantity en carrito

                this.setState({quantity: this.state.product.stock})
                // alert(`Sorry, today we only have ${this.state.product.stock}u. of stock for this reference. For more info please contact us at hola@ferrero.com`)

            }
        })
    }

   

    //tenemos que buscar en base de datos el id del producto y recuperar la informacion
    

    render() {
        console.log(this.props.match.params)
        return (
            <div className=''>
                <div className='product-view'>
                    <div className='product-view-img'>
                        <img src={this.state.product.img_link} />
                    </div>
                    <div className='product-view-info'>
                        <h1 className='h1 pb-1'>{this.state.product.name}</h1>
                        {/* <h1 className='h1'>Hola soy h1</h1>
                        <h1 className='h2'>Hola soy h2</h1>
                        <h1 className='h3'>Hola soy h3</h1>
                        <h1 className='h4'>Hola soy h4</h1>
                        <h1 className='h5'>Hola soy h5</h1> */}

                        <h2 className='pb-05'>{this.state.product.price}€</h2>
                        <p className='pb-1'>{this.state.product.description_short}</p>
                        <div className='d-center-flex pb-1'>

                            <div className='bold h4 noSelect'>
                                {/* <span className="pr-1 cursor-pointer" onClick={() => this.setState({ quantity: this.state.quantity -= 1 })}>-</span> */}
                                <span className="pr-1 cursor-pointer" onClick={() => this.addQuantity('-')}>-</span>
                                {this.state.quantity}
                                <span className="pl-1 pr-1 cursor-pointer" onClick={() => this.addQuantity('+')}>+</span>
                            </div>

                            <h4 className={'prod-item-button cursor-pointer'} onClick={()=>this.props.addToCart(this.props.match.params._id, this.state.quantity)}>add to cart</h4>

                        {/* <span>{this.state.product.stock} </span> */}
                    {/* <h4 onClick={this.addToCart}>add to cart</h4> */}
                            <FontAwesomeIcon size='2x' className='icon' icon={['far', 'heart']} onClick={this.props.addToWishList} />

                        </div>
                        <div className='share-links'>
                            <h4 className='d-inline'>Share:</h4>
                            <a href='#'><FontAwesomeIcon size='lg' className='icon' icon={['fab', 'facebook-f']} /></a>
                            <a href='#'><FontAwesomeIcon size='lg' className='icon' icon={['fab', 'whatsapp']} /></a>
                            <FontAwesomeIcon size='lg' className='icon' icon='share-alt' />
                        </div>
                        {/* TODO: EXAMPLE OF CONDITIONAL STATEMENT */}
                        <h4>{this.state.stock ? null : "This product is out of stock"}</h4>
                    </div>

                </div>

                <details>
                    <summary>Show something</summary>
                    <div>Something!</div>
                </details>

                <div className='more-info'>
                    <AccordionSemantic />
                </div>


                {/* <button class="ui button" role="button">Btn with Semantic-UI</button> */}
            </div>

        )
    }
}

export default Prod_View;