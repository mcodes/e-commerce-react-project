import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './components/Home'
import Footer from './components/footer/Footer';
import Header from './components/header/Header';
import AdminCreateCat from './pages/admin/createcat';
import AdminCreate from './pages/admin/create';
import AdminUpdate from './pages/admin/update';
// import AdminDashboard from './pages/admin/dashboard';
import AdminDashboard from './pages/admin/dashboard';
import SingleProduct from './pages/product_view';
import ShoppingCart from './components/shopping_cart/Shopping_Cart';
import WishList from './components/wish_list/Wish_List';
import Login from './components/login/Login';
import Register from './components/register/Register';
import Address from './components/Address';
import axios from 'axios';
import './App.css'




class App extends Component {

  state = {
    cart: [],
    wishListItems: [],
    cartListItems: [],
    total: 0,
    totalQuantityProduct: 0,
    products: [],
    cartQuantity: 0,
    isLoggedIn: false,
  }

    
// TODO: check with stefano this.setState({ cartListItems: cartLS.length })

  componentWillMount = () => {
    const wishListItems = JSON.parse(localStorage.getItem('wishList'))
    //recuperamos array de localstorage
    const cartItems = JSON.parse(localStorage.getItem('cart'))
    if (wishListItems === null) localStorage.setItem('wishList', JSON.stringify([]))
    if (cartItems === null) { localStorage.setItem('cart', JSON.stringify([])) }
    this.getData()
    this.getProducts()
    //controlamos si es cero no hacemos nada, sino nos da error
    if (cartItems !== null) {this.setState({ wishListItems: wishListItems.length, cartListItems: cartItems.length })}
    this.isLoggedIn()
  }

  //cuando comp se renderice we get the info from the db
  getProducts() {
    //get de url path de db
    axios.get(`http://localhost:8080/products/`)
      .then((res) => {
        console.log('res', res)
        //we are setting the state in home to res.data so we can send them to prodlist
        this.setState({ products: res.data })
      })
      .catch(e => {
        console.log('error', e)
      })
  }

  getData = () => {
    //recuperamos array cart del localstorage
    const cartItems = JSON.parse(localStorage.getItem('cart'))
    //con axios mandamos el array en localstorage para buscar info segun el id
    //cartItems el obj q contiene cartItems es el body del request que pasamos al server
    axios.post(`http://localhost:8080/products/cart_items`, { cartItems })
      //la respuesta del servidor el shoppingCartItemsInfo            del server====>   res.send({shoppingCartItemsInfo});
      .then((res) => {
        //antes de setear el state miramos la quantity
        res.data.shoppingCartItemsInfo.map((el, index) => {
          let prodIndex = cartItems.findIndex(ele => ele._id === el._id)
          //aqui le añadimos quantity a cartItems 
          el.quantity = cartItems[prodIndex].quantity;
          //get index of objext in array of local storage
          //with the index [].quantity
          //return quantity
          this.setState({ cartQuantity: cartItems[prodIndex].quantity })
          
        })

        this.setState({ cart: res.data.shoppingCartItemsInfo })
        this.getTotal()
        //this.setState({ total: total })
        //let quantity = cartItems[prodIndex].quantity;
      }).catch(e => {
        console.log('error', e)
      })
  }

  delete = (_id) => {
    let { cart } = this.state
    let index = cart.findIndex(ele => ele._id === _id)

    //remove from cart
    cart.splice(index, 1)
    //update state
    this.setState({ cart })
    
    const cartLS = JSON.parse(localStorage.getItem('cart'))
    let cartIndex = cartLS.findIndex(ele => ele._id === _id)
    
    //remove from cart
    cartLS.splice(cartIndex, 1)
    // update local storage
    localStorage.setItem("cart", JSON.stringify(cartLS))
    this.getTotal()
    this.setState({ cartListItems: cartLS.length })
  }



  add = (sign, _id) => {
    let { cart } = this.state
    let index = cart.findIndex(ele => ele._id === _id)

    //now we modify the quantity of id in localStorage
    // get cart 
    const cartLS = JSON.parse(localStorage.getItem('cart'))
    let cartIndex = cartLS.findIndex(ele => ele._id === _id)


    if (sign === "+" && cartLS[cartIndex].quantity + 1 > cart[index].stock) {
      alert('stock')
      return
    }
    
    cart[index].quantity = eval(`${cart[index].quantity} ${sign} 1 `)
    this.setState({ cart })
    this.setState({ cartListItems: cartLS.length })

    cartLS.map((el, index) => {
      let prodIndex = cartLS.findIndex(ele => ele._id === el._id)
      //aqui le añadimos quantity a cartLS

      el.quantity = cart[prodIndex].quantity;
      //get index of objext in array of local storage
      //with the index [].quantity
      //return quantity
    })
    //
    // if ( this.state.quantity  > this.state.product.stock) alert('ojo stock')
    // dind index 
    // change quantity
    // and set local stoarage
    localStorage.setItem("cart", JSON.stringify(cartLS))
    //llamamos a la funcion para que recalcule el total del shopping cart
    this.getTotal()
  }

  getTotal = () => {
    console.log('this.props from shopping cart', this.props)
    //var { total } = this.state;
    var total = 0;
    //console.log("this.state.prod+++++++++++++", this.state.products)

    this.state.cart.map((el, index) => {
      //console.log(el.price)
      //console.log(el.quantity)
      total += el.price * el.quantity
      // debugger
      return total
    })

    console.log('this.state.total from shopping cart', this.state.total)
    this.setState({ total: total })
  }



  addToCart = (_id, quantity = 1) => {
    //primero mirar si hay stock >0

    //if it doenst exist create local storage
    //parse lo convierte en array
    //con el or si el null q sea un empty array

    //now we modify the quantity of id in localStorage
    // get cart 
    let { products } = this.state
    let prodIndex = products.findIndex(ele => ele._id === _id)

    let cartItems = JSON.parse(localStorage.getItem('cart'))
    let cartIndex = cartItems.findIndex(ele => ele._id === _id)

    if (cartIndex === -1 && products[prodIndex].stock != 0) {
      alert('added')
      cartItems.push({ _id, quantity })
    } else {
      if (cartItems[cartIndex].quantity + quantity > products[prodIndex].stock) {
        alert('no stock')
      } else {
        alert('added')
        cartItems[cartIndex].quantity = cartItems[cartIndex].quantity + quantity
      }
    }

    localStorage.setItem("cart", JSON.stringify(cartItems))

    this.setState({ cartListItems: cartItems.length })





    //     localStorage.setItem("cart", JSON.stringify(cartItems))

    // TODO: PENDIENTE UPDATE STATE

    // let cart = this.state.cart
    // let index = cart.findIndex(ele => ele._id === _id)

    // if (cart[index].stock === 0) {
    //   alert('no stock')
    // } else {
    //   //if it doesnt exist, add item to localstorage cart id + quantity
    //   if (cartIndex === -1) {
    //     cartItems.push({ _id, quantity })
    //     localStorage.setItem("cart", JSON.stringify(cartItems))
    //   } else {
    //     cartItems[cartIndex].quantity = cartItems[cartIndex].quantity + quantity
    //     localStorage.setItem("cart", JSON.stringify(cartItems))
    //   }
    // }









    // axios.get(`http://localhost:8080/products/stock/${_id}/${quantity}`)
    //   .then((res) => {
    //     // if (res.data.ok) this.setState({product: res.data.productInfo, stock:res.data.stock})
    //     // if(res.data.stock) this.setState({quantity: amount+1}) 
    //     // if stock is true we go into function
    //     if (res.data.stock) {

    //       //checking index of id
    //       if (index === -1) {
    //         cartItems.push({ _id, quantity: quantity })
    //       } else {
    //         cartItems[index].quantity += quantity
    //       }
    //       let { cart } = this.state
    //       //console.log(this.state.cart)


    //       cartItems.map((el, index) => {
    //         let prodIndex = cartItems.findIndex(ele => ele._id === el._id)
    //         //aqui le añadimos quantity a cartLS 
    //         el.quantity = cartItems[prodIndex].quantity;
    //         //get index of objext in array of local storage
    //         //with the index [].quantity
    //         //return quantity
    //       })
    //       // dind index 
    //       // change quantity
    //       // and set local stoarage
    //       localStorage.setItem("cart", JSON.stringify(cartItems))
    //       this.setState({ cart })

    //       alert("added")

    //     } else {
    //       alert(`Sorry, today we only have ${res.data.stockNumber}u. of stock for this reference. For more info please contact us at hola@ferrero.com`)
    //     }

    //     //we are setting the state in home to res.data so we can send them to prodlist
    //     //this.setState({products: res.data})
    //   })
    //   .catch(e => {
    //     console.log('error', e)
    //   })

  }

  addToWishList = (_id) => {
    //let total = 0
    //========================================================================= this.setState({color: 'main-color'})
    //const { _id } = this.props;
    const wishListItems = JSON.parse(localStorage.getItem('wishList'))
    //const {quantity} = this.props;
    //if it doenst exist create local storage

    //buscamos el index para saber si existe el id
    //con get parse llamamos el local storage
    //con set lo seteamos (cons stringify)
    let index = wishListItems.findIndex(ele => ele._id === _id)
    if (index === -1) {
      wishListItems.push(_id)
      this.setState({ wishListItems })
    }
    //wishList seria la key, y el array es el valor. y le pasamos wishlistitems
    localStorage.setItem('wishList', JSON.stringify(wishListItems))
    console.log('this.state.wishListItems.length', this.state.wishListItems.length)
  }

  emptyCart = () => {
    localStorage.removeItem('cart')
    this.setState({ cart: [], total: 0 })
  }


  deleteProduct = (_id) => {
    axios.post('http://localhost:8080/products/delete', { _id })
      .then(res => {
        this.getProducts()

      })
      .catch(e => {
        console.log('error', e)
      })
  }



  isLoggedIn = () => {
    const token = JSON.parse(localStorage.getItem('authToken'));
        if (token != null) return this.setState({isLoggedIn: true});
        return this.setState({isLoggedIn: false},()=>console.log(this.state.isLoggedIn,'token--------------------------------------------------')
        );
        
  }




  render() {
    return <Router>
      {/* todos los routes automaticamente tienen las propiedades del router (history match etc) a no ser q le quiera pasar mas cosas
      en ese caso hacemos como el path de home */}
      <Header
        cartListItems={this.state.cartListItems}
        wishListItems={this.state.wishListItems}
        add={this.add}
        cart={this.state.cart}
        getTotal={this.getTotal}
        total={this.state.total}
        getData={this.getData}
        delete={this.delete}
        isLoggedIn={this.isLoggedIn}
        isLoggedInState={this.state.isLoggedIn}

      />
      {/* render={props => <About  isLoggedIn={this.isLoggedIn} />} */}
      <Route exact path='/' render={props =>
        <Home
          props={props}
          addToWishList={this.addToWishList}
          addToCart={this.addToCart}
          products={this.state.products}
        />} />
      <Route exact path='/product-view/:_id' render={props =>
        <SingleProduct
          {...props}
          addToWishList={this.addToWishList}
          addToCart={this.addToCart}
          cartQuantity={this.state.cartQuantity}
        // add={this.add}
        />} />
      {/* <Route path='/product-view/:_id' component={SingleProduct} /> */}

      <Route path='/admin/dashboard' render={(props) => (<AdminDashboard {...props} products={this.state.products} getProducts={this.getProducts} deleteProduct={this.deleteProduct} />)} />

      <Route path='/admin/create' render={(props) => (<AdminCreate {...props} products={this.state.products} getProducts={this.getProducts}  />)} />
      <Route path='/admin/createcat' render={(props) => (<AdminCreateCat {...props} />)} />

      <Route path='/admin/update/:_id' render={(props) => (<AdminUpdate {...props} products={this.state.products} getProducts={this.getProducts} />)} />

      {/* <Route path='/shopping-cart' component={ShoppingCart} /> */}
      <Route path='/wish-list' component={WishList} />
      <Route path='/register' component={Register} />
      <Route path='/login' render={() => (<Login isLoggedIn={this.isLoggedIn} />)} />
      <Route path='/address' render={(props) => (
        <Address
          {...props}
          add={this.add}
          delete={this.delete}
          total={this.state.total}
          cart={this.state.cart}
          emptyCart={this.emptyCart}
        />)} />

      <Footer />
    </Router>
  }
}
export default App;