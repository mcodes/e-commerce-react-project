# eCommerce

## CORE FUNCTIONALITY
* Register admin
* Login admin
* Logout admin
* Add products
* remove products
* update products
* show all products
* show one product
* add product to cart
* cart page
* remove product from cart
* increase product quantity in cart
* descrease product quantity in cart
* clear cart
* go to checkout page
* payment (stripe)
* send email confirmation
* update stock

## EXTRA  FUNCTIONALITY IDEAS
* create new admins (must be logged in as admin)
* Register user
* Login user
* Logout user
* update user profile
* see orders done by userID
* sort orders by date
* send confimation email user
* password reset
* add categories
* remove categories
* update categories
* create order
* admin dashboard
* show orders
* change order status from  sent /  sent
* group products by category
* filter products in price range
* combine filters
* sort products by price (asc / desc)
* sort products alphabetically  (asc / desc)

### Links:
* [Auth server](https://gitlab.com/barcelonacodeschool/Express_MONGO_Auth_JWT)
* [Auth Client](https://gitlab.com/barcelonacodeschool/React_js_User_Auth_JWT)

* [Mailer client](https://gitlab.com/barcelonacodeschool/react_contact_form_with_nodemailer)
* [Mailer server](https://gitlab.com/barcelonacodeschool/express_nodemailer_server)

* [Stripe payment (client and server)](https://gitlab.com/barcelonacodeschool/stripe_checkout_react_express)

* [Cloudinary image upload](https://gitlab.com/antonelloBarcelonacodeschool/cloudinary_example.git)
